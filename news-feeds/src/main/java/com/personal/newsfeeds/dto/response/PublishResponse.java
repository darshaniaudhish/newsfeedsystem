package com.personal.newsfeeds.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublishResponse {
    String status;

}
