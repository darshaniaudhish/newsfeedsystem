package com.personal.newsfeeds.publisher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class NewsPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publish(MultipartFile file){
        rabbitTemplate.convertAndSend("news-feeds","news1","test message");
    }

}
