package com.personal.newsfeeds.controller;

import com.personal.newsfeeds.constants.NewsArticle;
import com.personal.newsfeeds.dto.response.PublishResponse;
import com.personal.newsfeeds.publisher.NewsPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.print.attribute.standard.Media;

@Controller
public class PublishArticle {

    @Autowired
    NewsPublisher newsPublisher;

    @PostMapping(value = "/publishNews",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<PublishResponse> publishArticle(MultipartFile[] files){
        System.out.println(files.length + " file(s) received");
        System.out.println("Following files are uploaded");
        for(MultipartFile file: files){
            System.out.println(file.getOriginalFilename());
            newsPublisher.publish(file);
        }

        PublishResponse response = new PublishResponse();
        response.setStatus(NewsArticle.ACCEPTED);
        return new ResponseEntity<PublishResponse>(response,HttpStatus.ACCEPTED);
    }

}
