package com.personal.newsfeeds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsFeedProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsFeedProducerApplication.class, args);
	}

}
